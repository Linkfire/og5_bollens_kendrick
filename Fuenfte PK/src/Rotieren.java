
public class Rotieren {

	public Quadrat gravity(Quadrat quadrat) {
		String[][] inhalt = quadrat.getinhalt();
		int groesse = quadrat.getGroesse();
		String[][] newInhalt = new String[groesse][groesse];
		
		
		Quadrat rueckgabe = new Quadrat(newInhalt,groesse);
		return rueckgabe;
	}
	
	
	public boolean check(Quadrat quadrat) {
		String[][] inhalt = quadrat.getinhalt();

		// Unten überprüfen
		for (int i = 0; i < quadrat.getGroesse(); i++) {
			if (!inhalt[0][i].equals(" ") && !inhalt[0][i].equals("#")) {
				return true;
			}
		}

		// Oben überprüfen
		for (int i = 0; i < quadrat.getGroesse(); i++) {
			if (!inhalt[quadrat.getGroesse() - 1][i].equals(" ") && !inhalt[quadrat.getGroesse() - 1][i].equals("#")) {
				return true;
			}
		}

		// Rechte Wand überprüfen
		for (int i = 0; i < quadrat.getGroesse(); i++) {
			if (!inhalt[i][0].equals(" ") && !inhalt[i][0].equals("#")) {
				return true;
			}
		}

		// Linke Wand überprüfen
		for (int i = 0; i < quadrat.getGroesse(); i++) {
			if (!inhalt[i][quadrat.getGroesse() - 1].equals(" ") && !inhalt[i][quadrat.getGroesse() - 1].equals("#")) {
				return true;
			}
		}
		
		return false;
	}
}
