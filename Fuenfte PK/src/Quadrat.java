import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class Quadrat {

	// Attribute
	private String[][] inhalt;
	private int groesse;

	// Constructor
	public Quadrat(String dateipfad) {
		try {
			FileReader fr = new FileReader(dateipfad);
			BufferedReader br = new BufferedReader(fr);
			this.groesse = Integer.parseInt(br.readLine());
			this.inhalt = new String[this.groesse][this.groesse];
			for (int y = 0; y < this.groesse; y++) {
				String zeilex = br.readLine();
				for (int x = 0; x < this.groesse; x++) {
					this.inhalt[y][x] = "" + zeilex.charAt(x);
				}
			}
			br.close();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
			System.out.println("Datei konnte nicht gefunden werden");
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("Datei konnte nicht ausgelesen werden");
		}

	}
	
	public Quadrat(String[][] inhalt,int groesse) {
		this.inhalt = inhalt;
		this.groesse = groesse;
	}

	// Getter und Setter
	public String[][] getinhalt() {
		return inhalt;
	}

	public void setinhalt(String[][] inhalt) {
		this.inhalt = inhalt;
	}

	public int getGroesse() {
		return groesse;
	}

	// Methode zur Ausgabe im Terminal
	public void ausgabe() {
		for (int y = 0; y < groesse; y++) {
			System.out.println();
			for (int x = 0; x < groesse; x++) {
				System.out.print(this.inhalt[y][x]);
			}
		}
	}

}
