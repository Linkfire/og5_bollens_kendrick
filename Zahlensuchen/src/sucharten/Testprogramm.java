package sucharten;

import java.util.Scanner;

public class Testprogramm {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.println("Geben sie die Laenge des Arrays an  ");
		System.out.print("n= ");
		int anzahl = scan.nextInt();
		long[] a = SortedListGenerator.getSortedList(anzahl);
		System.out.println("Welche Zahl wollen sie suchen (letzte Zahl im Array(a),keine Zahl im Array(b),erste Zahl im Array(c))");
		System.out.print("Wahl= ");
		long gesuchteZahl;
		switch (scan.next()) {
		case "a":
			gesuchteZahl = a[anzahl - 1];
			break;
		case "b":
			gesuchteZahl = -1;
			break;
		case "c":
			gesuchteZahl = a[0];
			break;
		default:
			System.out.println("Falsche eingabe stelle suche auf letzte Zahl");
			gesuchteZahl = a[anzahl - 1];
			break;
		}

		// Lineare Suche
		System.out.println("lineare Suche:");
		long time1 = System.currentTimeMillis();
		if (LinearSuche.lineareSuche(a, gesuchteZahl) < 0) {
			System.out.println("Stelle nicht gefunden");
		} else {
			System.out.println("Stelle= " + LinearSuche.lineareSuche(a, gesuchteZahl));
		}

		System.out.println(System.currentTimeMillis() - time1 + "ms");

		// Binaere Suche
		System.out.println("binaere Suche:");
		long time2 = System.currentTimeMillis();
		if (Binaeresuche.binaereSuche(a, gesuchteZahl) < 0) {
			System.out.println("Stelle nicht gefunden");
		} else {
			System.out.println("Stelle= " + Binaeresuche.binaereSuche(a, gesuchteZahl));
		}
		System.out.println(System.currentTimeMillis() - time2 + "ms");
		scan.close();
	}

}
