package sucharten;

public class Binaeresuche {
	public static int binaereSuche(long liste[], long suchwert) {
		final int NICHT_GEFUNDEN = -1;
		int mitte;
		int links = 0;
		int rechts = liste.length - 1;

		while (links <= rechts) {
			mitte = links + ((rechts - links) / 2);

			if (liste[mitte] == suchwert)
				return mitte;
			else if (liste[mitte] > suchwert)
				rechts = mitte - 1;
			else
				links = mitte + 1;

		}
		return NICHT_GEFUNDEN;
	}
}