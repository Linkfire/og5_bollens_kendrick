package sucharten;

public class LinearSuche {
	public static int lineareSuche(long liste[], long suchwert) {
		int stelle = -1;
		for (int i = 0; i < liste.length; i++) {
			if (liste[i] == suchwert) {
				stelle = i;
				break;
			}
		}
		return stelle;
	}
}
