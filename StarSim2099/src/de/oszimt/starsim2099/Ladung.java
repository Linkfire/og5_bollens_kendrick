package de.oszimt.starsim2099;

/**
 * Write a description of class Ladung here.
 * 
 * @author (your name)
 * @version (a version number or a date)
 */
public class Ladung {

	// Attribute
	 private int posX;
	  private int posY;
	  private int masse;
	  private String typ;
	// Methoden

	  public int getPosX() {
	    return posX;
	  }

	  public int getPosY() {
	    return posY;
	  }

	  public int getMasse() {
	    return masse;
	  }

	  public String getTyp() {
	    return typ;
	  }

	  public void setPosY(int posY) {
	    this.posY = posY;
	  }

	  public void setPosX(int posX) {
	    this.posX = posX;
	  }

	  public void setMasse(int masse) {
	    this.masse = masse;
	  }

	  public void setTyp(String typ) {
	    this.typ = typ;
	  }

	// Darstellung
	public static char[][] getDarstellung() {
		char[][] ladungShape = { { '/', 'X', '\\' }, { '|', 'X', '|' }, { '\\', 'X', '/' } };
		return ladungShape;
	}
}