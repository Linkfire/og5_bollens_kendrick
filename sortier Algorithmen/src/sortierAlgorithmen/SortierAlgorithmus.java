package sortierAlgorithmen;

public class SortierAlgorithmus {

	// Quicksort Abteil
	public static void quicksorten(long[] a) {
		// Ausgabe Anfang Anfang
		System.out.println("Quicksort:");
		System.out.println("-----------");
		System.out.print("Start" + "\t\t{ ");
		for (int x = 0; x < a.length; x++) {
			if (x == a.length - 1) {
				System.out.println(a[x] + " }");
			} else {
				System.out.print(a[x] + " , ");
			}

		}
		// Ausgabe Anfang Ende

		int l = 0;
		int r = a.length - 1;
		quicksort(a, l, r);

		// Ausgabe Ende Anfang
		System.out.print("Ende" + "\t\t{ ");
		for (int x = 0; x < a.length; x++) {
			if (x == a.length - 1) {
				System.out.println(a[x] + " }");
			} else {
				System.out.print(a[x] + " , ");
			}
		}
		// Ausgabe Ende Ende
	}

	private static void quicksort(long a[], int l, int r) {
		if (l < r) {
			int q = partition(a, l, r);
			if (l < q - 1)
				quicksort(a, l, q - 1);
			if (q < r)
				quicksort(a, q, r);
		}
	}

	private static int partition(long a[], int l, int r) {
		int i = l, j = r;
		long help;
		long x = a[(l + r) / 2];

		while (i <= j) {
			while (a[i] < x)
				i++;
			while (a[j] > x)
				j--;
			if (i <= j) {
				
				help = a[i];
				a[i] = a[j];
				a[j] = help;

				// Ausgabe Zwischenschritt Anfang
				System.out.print(i + " <-> " + j + "\t\t{ ");
				for (int k = 0; k < a.length; k++) {
					if (k == a.length - 1) {
						System.out.println(a[k] + " }");
					} else {
						System.out.print(a[k] + " , ");
					}
				}
				// Ausgabe Zwischenschritt Ende
				
				i++;
				j--;


			}
		}
		;

		return i;
	}

	// Selectsort Abteil
	public static void selectsorten(long[] a) {
		// Ausgabe Anfang Anfang
		System.out.println("Selectsort:");
		System.out.println("-----------");
		System.out.print("Start" + "\t\t{ ");
		for (int x = 0; x < a.length; x++) {
			if (x == a.length - 1) {
				System.out.println(a[x] + " }");
			} else {
				System.out.print(a[x] + " , ");
			}

		}
		// Ausgabe Anfang Ende

		int l = 0;
		int r = a.length - 1;
		selectsort(a, l, r);

		// Ausgabe Ende Anfang
		System.out.print("Ende" + "\t\t{ ");
		for (int x = 0; x < a.length; x++) {
			if (x == a.length - 1) {
				System.out.println(a[x] + " }");
			} else {
				System.out.print(a[x] + " , ");
			}

		}
		// Ausgabe Ende Ende

	}

	private static void selectsort(long[] a, int l, int r) {
		for (int i = r; i >= l + 1; i--) {
			int q = l;
			for (int j = l + 1; j <= i; j++) {
				if (a[j] > a[q]) {
					q = j;
				}

			}
			long zwerg = a[q];
			a[q] = a[i];
			a[i] = zwerg;

			// Ausgabe Zwsischenschritt Anfang
			System.out.print(q + " <-> " + i + "\t\t{ ");
			for (int x = 0; x < a.length; x++) {
				if (x == a.length - 1) {
					System.out.println(a[x] + " }");
				} else {
					System.out.print(a[x] + " , ");
				}
			}
			// Ausgabe Zwsischenschritt Ende

		}
	}
}