package git_taschenrechner;

import java.util.Scanner;

public class TaschenrechnerTest {

	public static void main(String[] args) {

		Scanner myScanner = new Scanner(System.in);
		Taschenrechner ts = new Taschenrechner();

		int swValue;
		boolean nochm = false;
		do {
			//repeat?
			nochm = false;

			// Display menu graphics
			System.out.println("============================");
			System.out.println("|   MENU SELECTION DEMO    |");
			System.out.println("============================");
			System.out.println("| Options:                 |");
			System.out.println("|        1. Addieren       |");
			System.out.println("|        2. Subtrahieren   |");
			System.out.println("|        3. Multiplizieren |");
			System.out.println("|        4. Dividieren     |");
			System.out.println("|        5. Potenzieren    |");
			System.out.println("|        6. Wurzel ziehen  |");
			System.out.println("|        7. Exit           |");
			System.out.println("============================");
			System.out.print(" Select option: ");
			swValue = myScanner.next().charAt(0);
			// Switch construct
			switch (swValue) {

			case '1':
				System.out.println("Sie Haben Addieren gew�hlt.");
				System.out.println("geben sie nun die Zahlen ein die sie addieren moechten");
				System.out.print("1.Summand:");
				double a = myScanner.nextDouble();
				System.out.print("2.Summand:");
				double b = myScanner.nextDouble();
				System.out.println("Hier ihre Summe:");
				System.out.println(a + "+" + b + "=" + (ts.add(a, b)));
				break;
			case '2':
				System.out.println("Sie Haben Subtrahieren gew�hlt.");
				System.out.println("geben sie nun die Zahlen ein die sie subtrahieren moechten");
				System.out.print("Minuend:");
				double c = myScanner.nextDouble();
				System.out.print("Subtrahend:");
				double d = myScanner.nextDouble();
				System.out.println("Hier ihre Differenz:");
				System.out.println(c + "-" + d + "=" + (ts.sub(c, d)));
				break;
			case '3':
				System.out.println("Sie Haben Multiplizieren gew�hlt.");
				System.out.println("geben sie nun die Zahlen ein die sie multiplizieren moechten");
				System.out.print("1.Faktor:");
				double e = myScanner.nextDouble();
				System.out.print("2.Faktor:");
				double f = myScanner.nextDouble();
				System.out.println("Hier ihr Produkt:");
				System.out.println(e + "*" + f + "=" + (ts.mul(e, f)));
				break;
			case '4':
				System.out.println("Sie Haben Dividieren gew�hlt.");
				System.out.println("geben sie nun die Zahlen ein die sie dividieren moechten");
				System.out.print("Dividend:");
				double g = myScanner.nextDouble();
				System.out.print("Divisor:");
				double h = myScanner.nextDouble();
				System.out.println("Hier ihr Quotient:");
				System.out.println(g + "*" + h + "=" + (ts.div(g, h)));
				break;
			case '5':
				System.out.println("Sie Haben Potenzieren gew�hlt.");
				System.out.println("geben sie nun die Zahlen ein die sie potenzieren moechten");
				System.out.print("Zahl:");
				double i = myScanner.nextDouble();
				System.out.print("Exponent:");
				double j = myScanner.nextDouble();
				System.out.println("Hier ihr potenzierte Zahl:");
				System.out.println(i + "^" + j + "=" + (ts.pot(i, j)));
				break;
			case '6':
				System.out.println("Sie Haben Multiplizieren gew�hlt.");
				System.out.println("geben sie nun die Zahlen ein von der sie die 2.Wurzel ziehen moechten");
				System.out.print("Zahl:");
				double k = myScanner.nextDouble();
				System.out.println("Hier ihr Wurzel:");
				System.out.println("  ____");
				System.out.println(ts.wurz(k));
				break;
			case '7':
				System.exit(1);
				myScanner.close();
				break;

			default:
				System.out.println("Invalid selection");
				break; // This break is not really necessary
			}
			// repeat?
			System.out.println("");
			System.out.println("");
			System.out.println("nochmal? j/n");
			char test = myScanner.next().charAt(0);
			if (test == 'j') {
				nochm = true;
			}
			else {
				myScanner.close();
				System.exit(1);
			}
		} while (nochm);
	}

}
