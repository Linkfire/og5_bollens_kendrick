package git_taschenrechner;

public class Taschenrechner {

	public double add(double zahl1, double zahl2) {
		return zahl1 + zahl2;
	}
	
	public double sub(double zahl1, double zahl2){
		return zahl1 - zahl2;
	}
	
	
	public double mul(double zahl1, double zahl2){
		return zahl1 * zahl2;
	}

	public double div(double zahl1, double zahl2){
		return zahl1 / zahl2;	
	}
	
	public double pot(double zahl1, double zahl2){
		double zahl3 = 1;
		for (int i = 1; i <= zahl2; i++) {
		zahl3 = zahl3 * zahl1;	
		}
		return zahl3;	
	}
	
	public double wurz(double zahl1){
		return Math.sqrt(zahl1);	
	}
}