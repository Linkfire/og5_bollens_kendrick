package standart;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import java.awt.FlowLayout;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.text.ParseException;
import java.awt.event.ActionEvent;

public class Bericht extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private Logic c;

	/**
	 * Launch the application.
	 */
	

	/**
	 * Create the frame.
	 * @throws IOException 
	 * @throws ParseException 
	 * @throws NumberFormatException 
	 */
	public Bericht(Logic c) throws IOException, NumberFormatException, ParseException {
		this.c = c;
		Logic logisch = new Logic();
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.NORTH);
		
		JLabel lblNewLabel = new JLabel("Bericht");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 16));
		panel.add(lblNewLabel);
		
		JPanel panel_1 = new JPanel();
		contentPane.add(panel_1, BorderLayout.CENTER);
		panel_1.setLayout(new GridLayout(0, 2, 0, 0));
		
		JLabel lblNewLabel_3 = new JLabel("Anzahl Eintr\u00E4ge:");
		panel_1.add(lblNewLabel_3);
		
		JLabel lblNewLabel_4 = new JLabel(""+logisch.getList().size());
		panel_1.add(lblNewLabel_4);
		
		int dauer = 0;
		for (int i = 0; i < logisch.getList().size(); i++) {
			dauer = dauer + logisch.getList().get(i).getDauer();
		}
		
		JLabel lblNewLabel_1 = new JLabel("Dauer:");
		panel_1.add(lblNewLabel_1);
		
		JLabel lblNewLabel_5 = new JLabel(""+dauer);
		panel_1.add(lblNewLabel_5);
		
		JLabel lblNewLabel_2 = new JLabel("im Durchschnitt gelernt:");
		panel_1.add(lblNewLabel_2);
		double durchschnitt = 0;
		for (int i = 0; i < logisch.getList().size(); i++) {
			durchschnitt = (durchschnitt + logisch.getList().get(i).getDauer())/2;
		}
		
		JLabel lblNewLabel_6 = new JLabel(""+durchschnitt);
		panel_1.add(lblNewLabel_6);
		
		JPanel panel_2 = new JPanel();
		FlowLayout flowLayout = (FlowLayout) panel_2.getLayout();
		flowLayout.setAlignment(FlowLayout.RIGHT);
		contentPane.add(panel_2, BorderLayout.SOUTH);
		
		JButton btnNewButton = new JButton("Beenden");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		panel_2.add(btnNewButton);
	}

}
