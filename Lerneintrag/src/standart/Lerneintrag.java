package standart;

import java.util.Date;

public class Lerneintrag {

	// Attribute
	private Date datum;
	private String fach;
	private String beschreibung;
	private int dauer;

	//Methoden
	public String toString(){
		return this.datum.toString()+" "+this.fach+" "+this.beschreibung+" "+this.dauer;
	}
	
	// Konstruktor
	public Lerneintrag(Date datum, String fach, String beschreibung, int dauer) {
		this.setDatum(datum);
		this.setFach(fach);
		this.setBeschreibung(beschreibung);
		this.setDauer(dauer);
	}
	
	//getter und setter
	public Date getDatum() {
		return datum;
	}

	public void setDatum(Date datum) {
		this.datum = datum;
	}

	public String getFach() {
		return fach;
	}

	public void setFach(String fach) {
		this.fach = fach;
	}

	public String getBeschreibung() {
		return beschreibung;
	}

	public void setBeschreibung(String beschreibung) {
		this.beschreibung = beschreibung;
	}

	public int getDauer() {
		return dauer;
	}

	public void setDauer(int dauer) {
		this.dauer = dauer;
	}

}
