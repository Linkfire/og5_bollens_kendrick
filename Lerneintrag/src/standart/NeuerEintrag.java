package standart;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.GridLayout;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.FlowLayout;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.awt.event.ActionEvent;

public class NeuerEintrag extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField txf_Fach;
	private JTextField txf_Beschreibung;
	private JTextField txf_Dauer;
	private Logic logisch;

	/**
	 * Launch the application.
	 */

	/**
	 * Create the frame.
	 */
	public NeuerEintrag(Logic logisch) {
		this.logisch = logisch;
		setTitle("Neuer Eintrag");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);

		JPanel panel_1 = new JPanel();
		contentPane.add(panel_1, BorderLayout.CENTER);
		panel_1.setLayout(new GridLayout(0, 2, 0, 0));

		JLabel lbl_Fach = new JLabel("Fach");
		lbl_Fach.setFont(new Font("Tahoma", Font.PLAIN, 15));
		panel_1.add(lbl_Fach);

		txf_Fach = new JTextField();
		txf_Fach.setFont(new Font("Tahoma", Font.PLAIN, 13));
		panel_1.add(txf_Fach);
		txf_Fach.setColumns(10);

		JLabel lbl_Beschreibung = new JLabel("Beschreibung");
		lbl_Beschreibung.setFont(new Font("Tahoma", Font.PLAIN, 15));
		panel_1.add(lbl_Beschreibung);

		txf_Beschreibung = new JTextField();
		txf_Beschreibung.setFont(new Font("Tahoma", Font.PLAIN, 13));
		panel_1.add(txf_Beschreibung);
		txf_Beschreibung.setColumns(10);

		JLabel lbl_Dauer = new JLabel("Dauer");
		lbl_Dauer.setFont(new Font("Tahoma", Font.PLAIN, 15));
		panel_1.add(lbl_Dauer);

		txf_Dauer = new JTextField();
		txf_Dauer.setFont(new Font("Tahoma", Font.PLAIN, 13));
		panel_1.add(txf_Dauer);
		txf_Dauer.setColumns(10);

		JPanel panel_2 = new JPanel();
		FlowLayout flowLayout = (FlowLayout) panel_2.getLayout();
		flowLayout.setAlignment(FlowLayout.RIGHT);
		contentPane.add(panel_2, BorderLayout.SOUTH);

		JButton btn_save = new JButton("Speichern");
		btn_save.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				DateFormat df = new SimpleDateFormat("dd.MM.yyyy");

				try {
					logisch.addEintrag(new Lerneintrag(java.util.Calendar.getInstance().getTime(), txf_Fach.getText(),
							txf_Beschreibung.getText(), Integer.parseInt(txf_Dauer.getText())));
				} catch (NumberFormatException | IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				dispose();
			}
		});
		panel_2.add(btn_save);

		JButton btn_reset = new JButton("Reset");
		panel_2.add(btn_reset);

		JButton btn_aboard = new JButton("Abbrechen");
		btn_aboard.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		panel_2.add(btn_aboard);
	}

}
