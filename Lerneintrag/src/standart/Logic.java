package standart;


import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class Logic {
	
	private List<Lerneintrag> eintrag;
	private String learnerName;
	
	public Logic() throws NumberFormatException, IOException, ParseException {
		this.eintrag = Filecontroll.getLern();
		this.learnerName = Filecontroll.getlearnerName();
	}
	
	public String[][] toTable() throws NumberFormatException, IOException, ParseException {
		String[][] table = new String[this.eintrag.size()][4];
		for (int i = 0; i < this.eintrag.size(); i++) {
			Lerneintrag ug = this.eintrag.get(i);
			DateFormat df = new SimpleDateFormat("dd.mm.yyyy");
			table[i][0] = df.format(ug.getDatum());
			table[i][1] = ug.getFach();
			table[i][2] = ug.getBeschreibung();
			table[i][3] = "" + ug.getDauer();

		}
		return table;
	}

	public void neuerEintrag(){
		NeuerEintrag frame = new NeuerEintrag(this);
		frame.setVisible(true);
	}
	
	public void bericht() throws NumberFormatException, IOException, ParseException{
		Bericht frame = new Bericht(this);
		frame.setVisible(true);
	}
	
	public void addEintrag(Lerneintrag e) throws IOException{
		this.eintrag.add(e);
		Filecontroll.beschreiben(e);
	}
	
	//Getter und setter
	public  String getName() throws IOException {
		return this.learnerName;
	}
	
	public void setName(String name){
	this.learnerName = name;	
	}
	
	
	public  List<Lerneintrag> getList() throws IOException {
		return this.eintrag;
	}
	
	public void setList(List<Lerneintrag> eintrag){
		this.eintrag = eintrag;
	}

}
