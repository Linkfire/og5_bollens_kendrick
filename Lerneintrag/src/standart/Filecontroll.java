package standart;

import java.io.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class Filecontroll {

	public static String getlearnerName() throws IOException {
		File file = new File("miriam.dat");
		FileReader fr = new FileReader(file);
		BufferedReader br = new BufferedReader(fr);
		String titel = br.readLine();
		br.close();
		return titel;
	}

	public static void beschreiben(Lerneintrag lern) throws IOException {
		Writer schreiber;
		schreiber = new BufferedWriter(new FileWriter("miriam.dat", true));
		String d = new String();
		DateFormat df = new SimpleDateFormat("dd.mm.yyyy");
		d = "\r" + df.format(lern.getDatum());
		d = d + "\r" + lern.getFach();
		d = d + "\r" + lern.getBeschreibung();
		d = d + "\r" + lern.getDauer();
		schreiber.append(d);
		schreiber.close();
	}

	public static int wielang() throws IOException {
		File file = new File("miriam.dat");
		FileReader fr = new FileReader(file);
		BufferedReader br = new BufferedReader(fr);
		br.readLine();
		int i = 0;
		while (br.readLine() != null) {
			i++;
		}
		br.close();
		return i / 4;

	}

	public static List<Lerneintrag> getLern() throws IOException, NumberFormatException, ParseException {
		List<Lerneintrag> lern = new LinkedList<Lerneintrag>();
		File file = new File("miriam.dat");
		FileReader fr = new FileReader(file);
		BufferedReader br = new BufferedReader(fr);
		br.readLine();
		String datu = br.readLine();
		while (datu != null) {
			SimpleDateFormat df = new SimpleDateFormat("dd.mm.yyyy");
			Date datum = df.parse(datu);
			String fach = br.readLine();
			String beschreibung = br.readLine();
			int dauer = Integer.parseInt(br.readLine());
			Lerneintrag ug = new Lerneintrag(datum, fach, beschreibung, dauer);
			lern.add(ug);
			datu = br.readLine();
		}
		br.close();
		return lern;
	}

}
