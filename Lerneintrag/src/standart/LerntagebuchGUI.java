package standart;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.FlowLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JScrollPane;

import java.awt.Font;
import java.io.IOException;
import java.text.ParseException;

import javax.swing.JTable;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class LerntagebuchGUI extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTable table;
	private Logic logisch = new Logic();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					LerntagebuchGUI frame = new LerntagebuchGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * 
	 * @throws IOException
	 * @throws ParseException
	 * @throws NumberFormatException
	 */
	public LerntagebuchGUI() throws IOException, NumberFormatException, ParseException {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));

		JPanel pnl_oben = new JPanel();
		contentPane.add(pnl_oben, BorderLayout.NORTH);
		pnl_oben.setLayout(new BorderLayout(0, 0));

		JLabel lbl_Titel = new JLabel("Lerntagebuch von " + logisch.getName());
		lbl_Titel.setFont(new Font("Tahoma", Font.BOLD, 18));
		pnl_oben.add(lbl_Titel, BorderLayout.NORTH);

		JPanel pnl_unten = new JPanel();
		contentPane.add(pnl_unten, BorderLayout.SOUTH);
		pnl_unten.setLayout(new BorderLayout(0, 0));

		JPanel pnl_untenrechts = new JPanel();
		pnl_unten.add(pnl_untenrechts, BorderLayout.EAST);
		pnl_untenrechts.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));

		JButton btn_neuerEintrag = new JButton("neuer Eintrag");
		btn_neuerEintrag.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				logisch.neuerEintrag();
			}
		});
		pnl_untenrechts.add(btn_neuerEintrag);

		JButton btn_bericht = new JButton("Bericht");
		btn_bericht.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					logisch.bericht();
				} catch (NumberFormatException | IOException | ParseException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		
		JButton btn_reload = new JButton("Neu laden");
		btn_reload.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
				try {
					LerntagebuchGUI ltb = new LerntagebuchGUI();
					ltb.setVisible(true);
				} catch (NumberFormatException | IOException | ParseException e1) {
					e1.printStackTrace();
				}
			}
		});
		pnl_untenrechts.add(btn_reload);
		pnl_untenrechts.add(btn_bericht);

		JButton btn_beenden = new JButton("Beenden");
		btn_beenden.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		pnl_untenrechts.add(btn_beenden);

		JPanel panel_mitte = new JPanel();
		contentPane.add(panel_mitte, BorderLayout.CENTER);
		panel_mitte.setLayout(new BorderLayout(0, 0));

		JScrollPane scrollPane = new JScrollPane();
		panel_mitte.add(scrollPane, BorderLayout.CENTER);

		String[] s = { " Datum", "Fach", "Aktivitšten", "Dauer" };
		String[][] b = logisch.toTable();
		table = new JTable(b, s);
		scrollPane.setViewportView(table);
	}

	public void reload() throws NumberFormatException, IOException, ParseException {
		
	}

}
