package zooTycoon;

public class Addon {
	
	// Anfang Attribute
	  private String bezeichnung;
	  private int id;
	  private double preis;
	  private int bestand;
	  private int maxbestand;
	  // Ende Attribute
	  
	  public Addon() {
	    this.bezeichnung = "";
	    this.id = 0;
	    this.preis = 0;
	    this.bestand = 0;
	    this.maxbestand = 0;
	  }

	  public Addon(String bezeichnung, int id, double preis, int bestand, int maxbestand) {
	    this.bezeichnung = bezeichnung;
	    this.id = id;
	    this.preis = preis;
	    this.bestand = bestand;
	    this.maxbestand = maxbestand;
	  }

	  // Anfang Methoden
	  public String getBezeichnung() {
	    return bezeichnung;
	  }

	  public void setBezeichnung(String bezeichnung) {
	    this.bezeichnung = bezeichnung;
	  }

	  public int getId() {
	    return id;
	  }

	  public void setId(int id) {
	    this.id = id;
	  }

	  public double getPreis() {
	    return preis;
	  }

	  public void setPreis(double preis) {
	    this.preis = preis;
	  }

	  public int getBestand() {
	    return bestand;
	  }

	  public void setBestand(int bestand) {
	    this.bestand = bestand;
	  }

	  public int getMaxbestand() {
	    return maxbestand;
	  }

	  public void setMaxbestand(int maxbestand) {
	    this.maxbestand = maxbestand;
	  }
	  
	  public void verbrauchen(int anzahl){
		this.bestand -= anzahl;  
	  }
	  
	  public void kaufen(int anzahl){
		  this.bestand += anzahl;
	  }
	  public int maxbestandErmitteln(){
		  return this.maxbestand;
	  }
	  
	  // Ende Methoden
}
