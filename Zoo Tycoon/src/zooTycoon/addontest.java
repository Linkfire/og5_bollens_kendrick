package zooTycoon;

public class addontest {
	public static void main(String[] args) {
		Addon addon01 = new Addon("spastibastitothestars",4,1.45,1,1);
		System.out.println("-----------------------------------------");
		System.out.println("Bezeichnung: "+addon01.getBezeichnung());
		System.out.println("Preis: "+addon01.getPreis());
		System.out.println("ID: "+addon01.getId());
		System.out.println("Bestand: "+addon01.getBestand());
		System.out.println("Maximaler Bestand: "+addon01.getMaxbestand());
		System.out.println("-----------------------------------------");
		System.out.println("");
		Addon addon02 = new Addon("ottosheavyfart",6,2.45,3,6);
		System.out.println("-----------------------------------------");
		System.out.println("Bezeichnung: "+addon02.getBezeichnung());
		System.out.println("Preis: "+addon02.getPreis());
		System.out.println("ID: "+addon02.getId());
		System.out.println("Bestand: "+addon02.getBestand());
		System.out.println("Maximaler Bestand: "+addon02.getMaxbestand());
		System.out.println("-----------------------------------------");
	}
}
