package keyStore;

public class KeyStore01 {
	String[] array;

	public KeyStore01() {
		this.array = new String[100];
	}

	public KeyStore01(int length) {
		this.array = new String[length];
	}

	public boolean add(String eintrag) {
		for (int i = 0; i < this.array.length; i++) {
			if (this.array[i] == null) {
				this.array[i] = eintrag;
				return true;
			}
		}
		return false;
	}

	public void clear() {
		this.array = new String[this.array.length];
	}

	public String get(int index) {
		return this.array[index];
	}

	public int indexOf(String eintrag) {
		for (int i = 0; i < this.array.length; i++) {
			if (this.array[i].equals(eintrag)) {
				return i;
			}
		}
		return -1;
	}

	public void remove(int index) {
		this.array[index] = null;

	}

	public void remove(String eintrag) {
		for (int i = 0; i < this.array.length; i++) {
			if (this.array[i].equals(eintrag)) {
				this.array[i] = null;
			}
		}

	}

	public int size() {
		int size = 0;
		for (int i = 0; i < this.array.length; i++) {
			if (this.array[i] != null) {
				size++;
			}
		}
		return size;
	}

	public String toString() {
		String all = "";
		for (int i = 0; i < this.array.length; i++) {
			if (this.array[i] != null) {
				if (all.equals("")) {
					all = this.array[i];
				} else {
					all = all + this.array[i];
				}
			}
		}
		return all;
	}
}
