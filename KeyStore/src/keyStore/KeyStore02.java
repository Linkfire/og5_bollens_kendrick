package keyStore;

import java.util.*;

public class KeyStore02 {
	private LinkedList<String> array;

	public KeyStore02() {
		this.array = new LinkedList<String>();
	}

	public boolean add(String eintrag) {
		this.array.add(eintrag);
		return true;
	}

	public void clear() {
		this.array.clear();
	}

	public String get(int index) {
		return this.array.get(index);
	}

	public int indexOf(String eintrag) {
		
		return this.array.indexOf(eintrag)
;	}

	public void remove(int index) {
		this.array.remove(index);

	}

	public void remove(String eintrag) {
		this.array.remove(eintrag);
	}

	public int size() {
		return this.array.size();
	}

	public String toString() {
		return this.array.toString();
	}
}
