package rataion;

public class Loeser {
	private static boolean solved = false;

	public static void solve1(String[][] rahmen) {
		String ergebnis = tester(rahmen);
		if (ergebnis.charAt(0) == 'l' || ergebnis.charAt(0) == 'r') {
			System.out.print("die k�rzeste reihenfolge um es zu l�sen ist: ");
			for (int i = 0; i < ergebnis.length(); i++) {
				if (ergebnis.charAt(i) == 'l') {
					System.out.print("links ");
				} else {
					System.out.print("rechts ");
				}
			}
		} else {
			System.out.println("die k�rzeste reihenfolge um es zu l�sen ist " + tester(rahmen));
		}

	}

	public static void solved() {
		solved = true;
	}

	public static String tester(String[][] rahmen) {
		int moeglichkeiten = 1048576;// bei wie vielen m�glichkeiten er
										// testen soll
		for (int i = 0; i < moeglichkeiten; i++) {
			if (i == 0 || i == 1) {
				if (drehen("0", rahmen)) {

					return "r";
				}
				if (drehen("1", rahmen)) {
					return "l";
				}

			} else {
				if (drehen(toStringLR(Integer.toBinaryString(i)), rahmen)) {
					return toStringLR(Integer.toBinaryString(i));
				}
				if (drehen(toStringLR2(Integer.toBinaryString(i)), rahmen)) {
					return toStringLR2(Integer.toBinaryString(i));
				}

			}

		}
		return "nicht auffindbar bei " + moeglichkeiten + " Moeglichleiten";
	}

	public static String toStringLR2(String start) {
		String end = "";
		for (int i = start.length() - 1; i >= 0; i--) {
			if (start.charAt(i) == '0') {
				end = end + "l";
			} else {
				end = end + "r";
			}
		}
		return end;
	}

	public static String toStringLR(String start) {
		String end = "";
		for (int i = start.length() - 1; i >= 0; i--) {
			if (start.charAt(i) == '0') {
				end = end + "r";
			} else {
				end = end + "l";
			}
		}
		return end;
	}

	public static boolean drehen(String drehung, String[][] rahmen) {
		String[][] rahmenpruefung = new String[rahmen.length][rahmen.length];
		char art;
		for (int y = 0; y < rahmen.length; y++) {
			for (int x = 0; x < rahmen.length; x++) {
				rahmenpruefung[y][x] = rahmen[y][x];
			}
		}
		for (int i = 0; i < drehung.length(); i++) {
			art = drehung.charAt(i);
			if (art == 'l') {
				rahmenpruefung = Rotator.auf_die_linke_Seite_legen(rahmenpruefung);
			} else {
				rahmenpruefung = Rotator.auf_die_rechte_Seite_legen(rahmenpruefung);
			}
		}
		return solved;
	}

}
