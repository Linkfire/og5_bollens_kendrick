package rataion;

import java.io.*;

public class Start {
	public static void main(String[] args) throws IOException {
		System.out.println("Bitte endern sie den Pfad der Datei in der Methode read in der Klasse start");
		String[][] rahmen = read();
		Loeser.solve1(rahmen);
	}

	public static String[][] read() throws IOException {
		FileReader fr = new FileReader("D:/Dateien/Schule/Rahmen.txt");//pfad um aender je danach wo der Rahmen ist
		BufferedReader br = new BufferedReader(fr);
		String z1 = br.readLine();
		int arrayhb = Integer.parseInt(z1);
		String[][] rahmen = new String[arrayhb][arrayhb];
		for (int y = 0; y < arrayhb; y++) {
			String zx = br.readLine();
			for (int x = 0; x < arrayhb; x++) {
				rahmen[y][x] = "" + zx.charAt(x);
			}
		}
		br.close();
		return rahmen;
	}
}
