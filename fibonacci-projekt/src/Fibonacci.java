/* Fibonacci.java
   Programm zum Testen der rekursiven und der iterativen Funktion
   zum Berechnen der Fibonacci-Zahlen.
   AUFGABE: Implementieren Sie die Methoden fiboRekursiv() und fiboIterativ()
   HINWEIS: siehe Informationsblatt "Fibonacci-Zahlen oder das Kaninchenproblem"
   Autor:
   Version: 1.0
   Datum:
*/
public class Fibonacci {
	// Konstruktor
	Fibonacci() {
	}

	/**
	 * Rekursive Berechnung der Fibonacci-Zahl an n-te Stelle
	 * 
	 * @param n
	 * @return die n-te Fibonacci-Zahl
	 */
	long fiboRekursiv(int n) {
		if (n == 0) {
			return 0L;
		}
		if (n == 1) {
			return 1L;
		}
		return fiboRekursiv(n - 1) + fiboRekursiv(n - 2);
	}// fiboRekursiv

	/**
	 * Iterative Berechnung der Fibonacci-Zahl an n-te Stelle
	 * 
	 * @param n
	 * @return die n-te Fibonacci-Zahl
	 */
	long fiboIterativ(int n) {
		long ek = 0L;
		long jk = 1L;
		long zwerg;
		for (; n > 0; n--) {
			zwerg = ek;
            ek = jk + ek;
            jk = zwerg;
		}
		return ek;
	}// fiboIterativ

}// Fibonnaci
