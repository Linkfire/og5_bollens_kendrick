package primzahl;

public class Stoppuhr {
	// Atribute
	private long startpunkt = 0;
	private long endpunkt = 0;

	// Methoden
	public Stoppuhr(){

	}
	public void start() {
		this.startpunkt = System.currentTimeMillis();
	}

	public void stopp() {
		this.endpunkt = System.currentTimeMillis();
	}
	
	public void reset(){
		this.startpunkt = 0;
		this.endpunkt = 0;
	}
	
	public long getDauer(){
		return this.endpunkt - this.startpunkt;
	}
}
