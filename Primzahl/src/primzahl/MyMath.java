package primzahl;

/**
  *
  * Beschreibung
  *
  * @version 1.0 vom 04.07.2016
  * @author 
  */

public class MyMath {
  
  
  // Anfang Attribute
  // Ende Attribute
  
  // Anfang Methoden
  public static boolean isPrimzahl(long zahl) {
    boolean ist = true;
    if (zahl<2) {
      ist = false;
    } // end of if
    else {
      for (long i = 2;i<=(zahl/2)+1;i++ ) {
        if (zahl%i==0) {
          ist =false; 
          break;
        } // end of if
      }
    } // end of if-else
    
    return ist;
  }
}