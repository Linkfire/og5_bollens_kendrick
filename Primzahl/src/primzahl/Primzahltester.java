package primzahl;

import java.util.Scanner;

public class Primzahltester {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
			
			System.out.println("geben sie bitte eine Exponenten von 10 ein in dessen bereich sie ein Primzahl testen wollen");
			Primzahltester.primzahltesten(scan.nextInt());
			scan.close();
		
	}

	public static void primzahltesten(int n) {
		Stoppuhr stoppuhr = new Stoppuhr();
		long i = 0L;
		switch (n) {
		case 1:
			i = 7L;
			break;
		case 2:
			i = 13L;
			break;
		case 3:
			i = 113L;
			break;
		case 4:
			i = 10007L;
			break;
		case 5:
			i = 100019L;
			break;
		case 6:
			i = 1000003L;
			break;
		case 7:
			i = 10000019L;
			break;
		case 8:
			i = 100000081L;
			break;
		case 9:
			i = 1000000007L;
			break;
		case 10:
			i = 10000000033L;
			break;
		case 11:
			i = 100000000003L;
			break;
		default:
			System.out.println("Fehler keine g�ltige eingabe");
			break;
		}
		System.out.println("Testzahl ist: " + i);
		stoppuhr.start();
		MyMath.isPrimzahl(i);
		stoppuhr.stopp();
		System.out.println("Die Zeit betrug: " + stoppuhr.getDauer() + "ms oder " + ((double) stoppuhr.getDauer() / 1000.0) + "s");

	}

}
