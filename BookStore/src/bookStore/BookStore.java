package bookStore;

import java.util.*;

public class BookStore {
	private LinkedList<Book> booklist;

	public BookStore() {
		this.booklist = new LinkedList<Book>();
	}

	public void add(Book book){
		this.booklist.add(book);
	}

	public boolean add(String titel,String autor,String isbn){
		return this.booklist.add(new Book(titel,autor,isbn));
	}

	public int indexOf(String titel, String autor, String isbn) {
		
		
		return booklist.indexOf(new Book(titel,autor,isbn));
	}

}
