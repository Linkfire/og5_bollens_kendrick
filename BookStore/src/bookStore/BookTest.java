package bookStore;


public class BookTest {

	public static void main(String[] args) {

		Book b1 = new Book("Richard Helm", "Design Patterns", "ISBN-10:0201633612");
		Book b2 = new Book("Robert C. Martin", "Clean Code", "ISBN-10:0132350882");
		Book b3 = b1;
		
		System.out.println("b1:  "+b1);
		System.out.println("b2:  "+b2);
		System.out.println("b3:  "+b3);
		
		System.out.println("b1.equals(b2)-->"+b1.equals(b2));
		System.out.println("b1.equals(b3)-->"+b1.equals(b3));
		
		System.out.println("b1.compareTo(b3)-->"+b1.compareTo(b3));
		System.out.println("b1.compareTo(b2)-->"+b1.compareTo(b2));
		System.out.println("b2.compareTo(b1)-->"+b2.compareTo(b1));
	}

}
