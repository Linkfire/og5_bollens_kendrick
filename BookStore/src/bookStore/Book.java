package bookStore;

public class Book {
	private String titel;
	private String autor;
	private String isbn;

	public Book(String titel, String autor, String isbn) {
		this.setTitel(titel);
		this.setAutor(autor);
		this.setIsbn(isbn);
	}

	public String getTitel() {
		return titel;
	}

	public void setTitel(String titel) {
		this.titel = titel;
	}

	public String getAutor() {
		return autor;
	}

	public void setAutor(String autor) {
		this.autor = autor;
	}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public String toString() {
		return "Titel: " + this.titel + ", Autor: " + this.autor + ", " + this.isbn;
	}

	public int compareTo(Book book) {
		int a, b;
		for (int i = 0; i < this.isbn.length(); i++) {
			if (this.isbn.charAt(i) == '0' || this.isbn.charAt(i) == '1' || this.isbn.charAt(i) == '2'
					|| this.isbn.charAt(i) == '3' || this.isbn.charAt(i) == '4' || this.isbn.charAt(i) == '5'
					|| this.isbn.charAt(i) == '6' || this.isbn.charAt(i) == '7' || this.isbn.charAt(i) == '8'
					|| this.isbn.charAt(i) == '9') {
				a = (int) this.isbn.charAt(i);
				b = (int) book.getIsbn().charAt(i);
				if (b - a != 0) {
					return b - a;
				}
			}
		}
		return 0;
	}
	 public boolean equals(Book book){
		return this.isbn.equals(book.getIsbn());
	}

}
