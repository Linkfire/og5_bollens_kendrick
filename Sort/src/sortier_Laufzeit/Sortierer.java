package sortier_Laufzeit;

public class Sortierer {
	
	
	//quicksort Abteil
	public static void quicksortnow(int[] a){
		 qsort(a,0,a.length-1);
		
	}
	public static void qsort(int[] a,int links, int rechts) {
		
		if (links < rechts) {
	         int i = partition(a,links,rechts);
	         qsort(a,links,i-1);
	         qsort(a,i+1,rechts);
	      }
	   }
	    
	   public static int partition(int a[], int links, int rechts) {
	      int pivot, i, j, zw;
	      pivot = a[rechts];               
	      i     = links;
	      j     = rechts-1;
	      while(i<=j) {
	         if (a[i] > pivot) {     
	            // tausche x[i] und x[j]
	            zw = a[i]; 
	            a[i] = a[j]; 
	            a[j] = zw;                             
	            j--;
	         } else i++;            
	      }
	      // tausche x[i] und x[rechts]
	      zw      = a[i];
	      a[i]      = a[rechts];
	      a[rechts] = zw;
	        
	      return i;
	   }
	          
	       
	
}
