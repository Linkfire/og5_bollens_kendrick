package ticktacktoe;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.GridLayout;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class TictackToe extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 
	 */
	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TictackToe frame = new TictackToe();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public TictackToe() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new GridLayout(3, 3, 0, 0));
		
		JButton btn_1 = new JButton("");
		btn_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (btn_1.getText().equals("")) {
					btn_1.setText(XorO.getXorO());
				}
			}
		});
		contentPane.add(btn_1);
		
		JButton btn_2 = new JButton("");
		btn_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (btn_2.getText().equals("")) {
					btn_2.setText(XorO.getXorO());
				}
			}
		});
		contentPane.add(btn_2);
		
		JButton btn_3 = new JButton("");
		btn_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (btn_3.getText().equals("")) {
					btn_3.setText(XorO.getXorO());
				}
			}
		});
		contentPane.add(btn_3);
		
		JButton btn_4 = new JButton("");
		btn_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (btn_4.getText().equals("")) {
					btn_4.setText(XorO.getXorO());
				}
			}
		});
		contentPane.add(btn_4);
		
		JButton btn_6 = new JButton("");
		btn_6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (btn_6.getText().equals("")) {
					btn_6.setText(XorO.getXorO());
				}
			}
		});
		
		JButton btn_5 = new JButton("");
		btn_5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (btn_5.getText().equals("")) {
					btn_5.setText(XorO.getXorO());
				}
			}
		});
		contentPane.add(btn_5);
		contentPane.add(btn_6);
		
		JButton btn_7 = new JButton("");
		btn_7.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (btn_7.getText().equals("")) {
					btn_7.setText(XorO.getXorO());
				}
			}
		});
		contentPane.add(btn_7);
		
		JButton btn_8 = new JButton("");
		btn_8.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (btn_8.getText().equals("")) {
					btn_8.setText(XorO.getXorO());
				}
			}
		});
		contentPane.add(btn_8);
		
		JButton btn_9 = new JButton("");
		btn_9.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (btn_9.getText().equals("")) {
					btn_9.setText(XorO.getXorO());
				}
			}
		});
		contentPane.add(btn_9);
	}

}
